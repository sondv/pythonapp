echo 'deploy app to server Test: =======>'
rm -fr $HOME/pythonapp
mkdir -p $HOME/pythonapp
cd $HOME/pythonapp
git clone https://gitlab.com/sondv/pythonapp.git .

docker pull registry.gitlab.com/sondv/pythonapp:latest

sudo docker-compose down
sudo docker-compose up -d
echo '=====> deploy success on Test server'
